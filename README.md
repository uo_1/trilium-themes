# TokyoNight  Theme


## Usage

 - Create a CSS code note in Trilium and name it `Tokyonight Moon` 

 - Copy the content `Tokyonight Moon.css` 

 - Add the `#appTheme=Tokyonight `  attribute to it 

 - Download the `icon-blue01-png` , then right-click on the note and select Import to note 

 - Add the attribute `#originalFileName=icon-blue01.png #customResourceProvider=icon-blue01.png`  to `icon-blue01-png` 

 - Go to Menu > Options, and select `Tokyonight Moon` as your new theme 

 - Ctrl + R to reload 



## Inspiration 

This theme was design to be clean and minimal. 
Inspiration came from the following persons who I modify to create my own theme. 

- https://github.com/SiriusXT/trilium-theme-blue 
- https://github.com/cwilliams5/Midnight-Trilium-Dark-Mode 

## Preview 

![TokyoNight Screenshot](/screenshot.png?raw=true "TokyoNight Screenshot") 

## License
For open source projects, say how it is licensed.

